import pytest
import datetime

from flaskr.wsgi import app
from flaskr.extensions import db as _db
from flaskr.settings import TestingConfig
from flaskr.departments.models import DepartmentModel
from flaskr.employees.models import EmployeeModel


@pytest.yield_fixture(scope='function')
def _app():
    """An application for the tests."""
    app.config.from_object(TestingConfig)

    with app.app_context():
        _db.create_all()
        new_dep = DepartmentModel(name='Crewing')
        new_emp = EmployeeModel(
            department_id=1,
            dob=datetime.datetime.strptime("2020-12-12", '%Y-%m-%d'),
            name="TEST0",
            salary=1000.0
        )
        _db.session.add(new_dep)
        _db.session.add(new_emp)
        _db.session.commit()

    ctx = app.test_request_context()
    ctx.push()

    yield app.test_client()

    _db.session.close()
    _db.drop_all()
    ctx.pop()




