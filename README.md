### Department App

![](main_page.jpg)

## Aim
The idea of the project is to polish knowledge from past lectures
and get familiar with the full process of web development


## Project targets Maritime industry

## Features
- Add department/employee
    - checks if already exists, for employee - checks all parameters
- Remove department/employee
- Edit department/employee
    - checks if exist, doesn't create a new one
- Link employee to department
    - employee can have department NOT CHOSEN
    - employee exists when department deleted
- Search/filter employees
    - search by name/date of birth/ between date/ department
    - search by all above
- Get JSON of department/employee

## Access

Project deployed on heroku at 
https://dep-emp-app.herokuapp.com/



