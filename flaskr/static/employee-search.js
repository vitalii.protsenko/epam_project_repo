
// RESET FORM
function reset_form() {
    document.getElementById('date1').value = '';
    document.getElementById('search-f').reset();
    document.getElementById('date2').value = '';
    document.getElementById('date2').style.display = 'none';
}

// HIDE 2ND PICKER
if (document.getElementById('date1').value === "") {
    document.getElementById('date2').style.display = 'none';
}
document.getElementById('date1').addEventListener('change', function () {
  document.getElementById('date2').style.display = 'block';
})

// SET MAX VALUE TO DATEPICKER
var today = new Date().toISOString().split('T')[0];
document.getElementsByClassName("picker")[0].setAttribute('max', today);
document.getElementsByClassName("picker")[1].setAttribute('max', today);

