from flask import Blueprint, render_template, g

blueprint = Blueprint('home', __name__)


@blueprint.route('/')
def home():
    g.home = True
    return render_template('home.html')

