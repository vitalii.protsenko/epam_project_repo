from flask import Flask

from flaskr.extensions import db, migrate, ma, api, csrf
from flaskr.settings import DevelopmentConfig, ProductionConfig
from flaskr import departments, employees, home_view


def create_app(config_obj=DevelopmentConfig):
    """
    Application Factory
    :param config_obj: Setting Profile
    :return: app itself
    """
    app = Flask(__name__)
    app.config.from_object(config_obj)

    if not app.debug:
        import logging
        from logging.handlers import RotatingFileHandler
        file_handler = RotatingFileHandler('logs/department.log', 'a', 1 * 1024 * 1024, 10)
        file_handler.setFormatter(
            logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
        app.logger.setLevel(logging.INFO)
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)
        app.logger.info('dep-app startup')

    register_blueprints(app)
    register_apis(api)

    # initialize all apps
    api.init_app(app)
    csrf.init_app(app)
    db.init_app(app)
    ma.init_app(app)
    migrate.init_app(app, db)

    with app.app_context():
        db.create_all()

    return app


def register_apis(_api):
    """Register Flask resources."""
    _api.add_resource(departments.api.Department, '/api/department/<int:_id>', endpoint='single')
    _api.add_resource(departments.api.Department, '/api/department/new', endpoint='post-department')
    _api.add_resource(departments.api.DepartmentList, '/api/departments/', endpoint='all-deps')
    _api.add_resource(employees.api.Employee, '/api/employee/<int:_id>', endpoint='single-employee')
    _api.add_resource(employees.api.Employee, '/api/employee/new', endpoint='post-employee')
    _api.add_resource(employees.api.EmployeeList, '/api/employees/', endpoint='all-emps')
    _api.add_resource(employees.api.EmployeeList, '/api/search/employees/', endpoint='filter')


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(departments.views.blueprint)
    app.register_blueprint(employees.views.blueprint)
    app.register_blueprint(home_view.blueprint)
