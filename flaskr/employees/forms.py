from datetime import datetime
from flask_wtf import FlaskForm
from wtforms import StringField, FloatField, SubmitField, SelectField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, ValidationError


class EmployeeCreateForm(FlaskForm):
    name = StringField('Name', _name='name', validators=[DataRequired()])
    dob = DateField('Date of birth', _name='dob', format='%Y-%m-%d', validators=[DataRequired()])

    def validate_dob(self, field):
        if field.data <= datetime.today().date():
            return
        raise ValidationError('Date must be in the past')

    salary = FloatField('Salary', _name='salary', default=0.0)
    department = SelectField('Department', coerce=int)
    submit = SubmitField('Add')


class EmployeeEditForm(EmployeeCreateForm):
    submit = SubmitField('Edit')


class EmployeeSearchForm(FlaskForm):
    name = StringField('Name', _name='name', default='')
    date1 = DateField('Date of birth / From', _name='date1', format='%Y-%m-%d', default='')
    date2 = DateField('To', _name='date2', format='%Y-%m-%d', default='')
    department = SelectField('Department', coerce=str)
    submit = SubmitField('Search')
