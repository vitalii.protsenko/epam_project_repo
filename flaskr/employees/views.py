import requests

from flask import Blueprint, render_template, request, redirect, url_for, flash

from flaskr.employees.forms import (EmployeeCreateForm,
                                    EmployeeEditForm, EmployeeSearchForm)
from werkzeug.datastructures import MultiDict

BASE_URL = 'https://dep-emp-app.herokuapp.com'

blueprint = Blueprint('employees', __name__)


@blueprint.route('/employees', methods=['GET', 'POST'])
def display_employees():
    employees = requests.get(BASE_URL + '/api/employees/').json()
    departments = requests.get(BASE_URL + '/api/departments/').json()
    filtered = []
    # search-form
    select_choices = [('', '--select field--'), (0, 'No department')] + [(dep['id'], dep['name']) for dep in
                                                                         departments]
    # set default values
    form = EmployeeSearchForm()
    form.department.choices = select_choices

    if request.method == 'POST':
        request_url = BASE_URL + '/api/search/employees/?'
        if request.form.get('name'):
            request_url += 'name=' + request.form.get('name')
        if request.form.get('date1'):
            request_url += '&date1=' + request.form.get('date1')
        if request.form.get('date2'):
            request_url += '&date2=' + request.form.get('date2')
        if request.form.get('department'):
            request_url += '&department=' + request.form.get('department')
        response = requests.get(request_url)
        if response.status_code != 404 and hasattr(response, "__iter__"):
            filtered = response.json()
        else:
            filtered = response
    return render_template('employees.html', employees=employees, filtered=filtered, form=form, departments=departments)


# SINGLE
@blueprint.route('/employee/<_id>', methods=['GET', 'POST'])
def display_single_employee(_id):
    req = requests.get(BASE_URL + '/api/employee/' + _id)
    employee = req.json()
    if req.status_code == 404:
        return employee
    employee = req.json()
    departments = requests.get(BASE_URL + '/api/departments/').json()
    select_choices = [(0, 'Not chosen')] + [(dep['id'], dep['name']) for dep in departments]

    # set default values
    form = EmployeeEditForm(formdata=MultiDict(employee))
    form.department.choices = select_choices
    # default for select field
    form.department.data = employee['department_id'] if employee['department_id'] else 0
    # EDIT-FORM
    if request.method == 'POST' and form.validate_on_submit():
        flash('Employee updated', 'success')
        department_id = request.form.get('department')
        payload = {'name': request.form.get('name'),
                   'salary': request.form.get('salary'),
                   'dob': request.form.get('dob'),
                   'department_id': None if department_id == '0' else department_id}

        requests.put(BASE_URL + '/api/employee/' + _id, json=payload)
        return redirect(url_for('employees.display_employees'))
    elif request.method != 'GET':
        for error in form.errors.values():
            flash(error[0], 'danger')

    return render_template('employee.html', employee=employee, form=form, departments=departments)


@blueprint.route('/new/employee/', methods=['GET', 'POST'])
def create_employee():
    employees = requests.get(BASE_URL + '/api/employees/').json()
    departments = requests.get(BASE_URL + '/api/departments/').json()
    # form-create
    form = EmployeeCreateForm()
    select_choices = [(0, 'Not chosen')] + [(dep['id'], dep['name']) for dep in departments]
    form.department.choices = select_choices
    if request.method == 'POST' and form.validate_on_submit():
        flash('Employee created!', 'success')
        department_id = request.form.get('department')
        payload = {'name': request.form.get('name'),
                   'salary': request.form.get('salary'),
                   'dob': request.form.get('dob'),
                   'department_id': None if department_id == '0' else department_id}

        requests.post(BASE_URL + '/api/employee/new', json=payload)
        return redirect(url_for('employees.display_employees'))
    elif request.method != 'GET':
        for error in form.errors.values():
            flash(error[0], 'danger')

    return render_template('create_employee.html', employees=employees, departments=departments, form=form)
