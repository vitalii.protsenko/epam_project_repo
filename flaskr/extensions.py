"""Extensions module. Each extension is initialized in the app factory located in init.py."""

from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CSRFProtect


csrf = CSRFProtect()
api = Api(decorators=[csrf.exempt])
db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
