import requests

from flask import Blueprint, render_template, request, redirect, url_for, flash

from flaskr.departments.forms import DepartmentCreateForm, DepartmentEditForm

BASE_URL = 'https://dep-emp-app.herokuapp.com'
#BASE_URL = 'http://127.0.0.1:5000'

blueprint = Blueprint('departments', __name__)


@blueprint.route('/departments', methods=['GET', 'POST'])
def display_departments():
    """
    Takes all data from API
    :return: list of departments plus Create form
    """
    departments = requests.get(BASE_URL + '/api/departments/').json()
    employees = requests.get(BASE_URL + '/api/employees/').json()
    form = DepartmentCreateForm()
    if request.method == 'POST' and form.validate_on_submit():
        payload = {'name': request.form.get('name')}
        requests.post(BASE_URL + '/api/department/new', json=payload)
        flash('Department Created!', 'success')
        return redirect(url_for('departments.display_departments'))

    return render_template('departments.html', departments=departments, employees=employees, form=form)


@blueprint.route('/department/<_id>', methods=['GET', 'POST'])
def display_single_department(_id):
    """

    :param _id: id from DB
    :return:
    """
    req = requests.get(BASE_URL + '/api/department/' + _id)
    department = req.json()
    if req.status_code != 200:
        return department
    req_e = requests.get(BASE_URL + '/api/search/employees/?&department=' + str(department['id']))
    if req_e.status_code == 404:
        employees = req_e
    else:
        employees = req_e.json()
    form = DepartmentEditForm()
    if request.method == 'POST' and form.validate_on_submit():
        flash('Department Edited!', 'success')
        payload = {'name': request.form.get('name')}
        requests.put(BASE_URL + '/api/department/' + _id, json=payload)
        return redirect(url_for('departments.display_single_department', _id=_id))
        # maybe to make a redirect to a new object page

    return render_template('department.html', department=department, employees=employees, form=form)
