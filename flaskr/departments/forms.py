from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class DepartmentCreateForm(FlaskForm):
    name = StringField('Name', _name='name', validators=[DataRequired()])
    submit = SubmitField('Add')


class DepartmentEditForm(DepartmentCreateForm):
    submit = SubmitField('Edit')